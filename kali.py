# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import time
import sys

from neopixel import *
import random


# LED strip configuration:
LED_COUNT      = 358      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level value)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering

COLORS = []



COLORS.append(Color(255, 0, 0))
COLORS.append(Color(240, 0, 0))
COLORS.append(Color(225, 0, 0))
COLORS.append(Color(210, 0, 0))
COLORS.append(Color(195, 0, 0))
COLORS.append(Color(180, 0, 0))
COLORS.append(Color(165, 0, 0))
COLORS.append(Color(150, 0, 0))
COLORS.append(Color(135, 0, 0))

def flatColor(strip, color):
    for i in range(strip.numPixels()):
        strip.setPixelColor(strip.numPixels() - i, color)
        strip.show()
        time.sleep(0.005)

def stripes(strip, colors):
    color_order = []
    for i, color in enumerate(colors):
        if i % 2 == 0:
            color_order.append(color)
    for i, color in enumerate(colors):
        if i % 2 == 1:
            color_order.append(color)
    stripe_size = int(strip.numPixels()/len(colors))
    current_pos = 0
    number_of_loops = 0

    while True:
        current_pos = number_of_loops
        for color in color_order:
            for i in range(stripe_size):
                strip.setPixelColor(current_pos % strip.numPixels(), color)
                current_pos += 1
        number_of_loops += 1
        strip.show()


# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    stripes(strip, COLORS)
    exit(0)

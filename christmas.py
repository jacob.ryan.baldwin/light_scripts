# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import time
import sys

from neopixel import *
import random


# LED strip configuration:
LED_COUNT      = 358      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level value)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering

colors = [[255,0,0],[0,255,0],[225,255,255]]
NUMBER_OF_BLOCKS = 36
LENGTH_OF_BLOCK = int(LED_COUNT/NUMBER_OF_BLOCKS)

def flatColor(strip, color):
    for i in range(strip.numPixels()):
        strip.setPixelColor(strip.numPixels() - i, color)
        strip.show()
        time.sleep(0.02)

def clip(pos):
    if pos >= LED_COUNT:
        pos-= LED_COUNT
    return pos

def render(strip, offset):
    for i in range(NUMBER_OF_BLOCKS):
        progress = clip(offset + i*LENGTH_OF_BLOCK)
        color = colors[i%3]
        color = Color(int(color[0]),int(color[1]),int(color[2]))
        for j in range(LENGTH_OF_BLOCK):
            strip.setPixelColor(progress+j, color)

# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
    # Intialize the library (must be called once before other functions).
    strip.begin()
    offset = 0

    if len(sys.argv) > 1:
        delay = float(sys.argv[1])
    else:
        delay = 0.2

    while True:
        offset += 1
        offset = clip(offset)
        render(strip, offset)
        strip.show()
        time.sleep(delay)
    exit(0)

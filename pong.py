# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import time

from neopixel import *
import random


# LED strip configuration:
LED_COUNT      = 358      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering

class Ball:
    def __init__(self, strip, starting_pos=0, color=Color(255,255,255), speed=1, direction=1, width=1):
        self.color = color
        self.pos = starting_pos
        self.speed = speed
        self.direction = direction
        self.width = width

    def clear(self):
        for i in range(self.width):
            strip.setPixelColor(self.pos + i, Color(0, 0, 0))

    def withinBounds(self, pos):
        if pos >= self.pos and pos <= self.pos + self.width:
            return True
        else:
            return False

    def checkCollisions(self, other_ball):
        if self.direction == 1 and other_ball.withinBounds(self.pos + self.width):
            self.randomizeColor()
            if other_ball.direction == self.direction:
                other_ball.speed += 1
            self.direction = -1
            self.randomlyChangeSpeed()

        elif self.direction == -1 and other_ball.withinBounds(self.pos):
            self.randomizeColor()
            if other_ball.direction == self.direction:
                other_ball.speed += 1
            self.direction = 1
            self.randomlyChangeSpeed()

    def advance(self):
        if self.pos >= strip.numPixels() - self.width:
            self.direction = -1
        if self.pos <= 0:
            self.direction = 1
        self.pos = self.pos + self.speed * self.direction

    def draw(self):
        for i in range(self.width):
            strip.setPixelColor(self.pos + i, self.color)

    def randomlyChangeSpeed(self):
        self.speed = self.speed + random.randint(-1,1)
        if self.speed == 0:
            self.randomlyChangeSpeed()

        if self.speed > self.width/4:
            self.speed = self.speed - 2

    def randomizeColor(self):
        self.color = Color(random.randint(0,255), random.randint(0,255), random.randint(0,255))





def clear(strip):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0, 0, 0))
    strip.show()

def initBalls(strip, balls, n):
    standard_width = int(strip.numPixels()/3/n)
    for i in range(n):
        balls.append(Ball(strip, width=standard_width, starting_pos=int(i*1.5*standard_width)))

# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
    # Intialize the library (must be called once before other functions).
    strip.begin()


    balls = []

    initBalls(strip, balls, 8)

    while True:
        for ball in balls:
            ball.clear()
        for ball in balls:
            ball.advance()
        for ball in balls:
            ball.draw()
        strip.show()
        for ball in balls:
            for other_ball in balls:
                if other_ball != ball:
                    ball.checkCollisions(other_ball)
        time.sleep(0.01)
